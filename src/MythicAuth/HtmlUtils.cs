using Microsoft.Extensions.Hosting;
using System.Linq;

namespace MythicAuth
{
    public static class HtmlUtils
    {        
        public static string clientUri { get; set; }

        public static void configure(IHostEnvironment env) {
            string targetId = null;
            if(env.IsDevelopment())
            {
                targetId = "MythicTableLocal";
            }else
            {
                targetId = "MythicTableVueFrontEnd";
            }
            clientUri = Config.Clients.Where(c => c.ClientId == targetId)
                .FirstOrDefault().ClientUri;
        }
    }
}